import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from './store';

import App from './components/App';

import './scss/style.scss';

render(
  <React.StrictMode>
    <Provider store={store}>
      <Router basename="dev/mynotes/">
        <App />
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('notes-app')
);
