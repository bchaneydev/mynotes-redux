import { createSlice } from '@reduxjs/toolkit';

export const notesSlice = createSlice({
  name: 'notes',
  initialState: {
    notes: [
      {
        title: 'Shopping list',
        description: 'Apples, pears, carrots, scotch fillet steaks.',
        noteid: 1
      },
      {
        title: 'Passage from 1984',
        description:
          '"Who controls the past," ran the Party slogan, "controls the future: who controls the present controls the past. " And yet the past, though of its nature alterable, never had been altered. Whatever was true now was true from everlasting to everlasting.',
        noteid: 2
      },
      {
        title: 'My todo list',
        description: 'Start running 3x week and eating healthier.',
        noteid: 3
      }
    ]
  },
  reducers: {
    addNote: (state, action) => {
      console.log('add');
      state.notes = [...state.notes, action.payload];
    },
    editNote: (state, action) => {
      console.log('edit');
      const unchangedNotes = state.notes.filter(
        (note) => note.noteid !== action.payload.noteid
      );

      const notes = [action.payload, ...unchangedNotes];
      state.notes = notes.sort((a, b) => a.noteid - b.noteid);
    },
    deleteNote: (state, action) => {
      console.log('delete', action.payload);

      const filteredNotes = state.notes.filter(
        (note) => note.noteid !== action.payload
      );

      state.notes = [...filteredNotes];
    }
  }
});

export const { addNote, editNote, deleteNote } = notesSlice.actions;

export default notesSlice.reducer;
