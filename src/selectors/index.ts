import { RootState } from '../store';
import { createSelector } from 'reselect';

const getNotes = (state: RootState) => state.notes.notes;

export const selectNotes = createSelector([getNotes], (getNotes) => getNotes);
