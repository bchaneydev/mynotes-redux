export type Note = {
  title: string;
  description: string;
  noteid: number;
};
