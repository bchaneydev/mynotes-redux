import React from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { addNote } from '../../../reducers/Notes';
import { selectNotes } from '../../../selectors';

import './style.scss';

type Props = {
  title: string;
  children?: React.ReactNode;
};

const Button = ({ title, children }: Props) => {
  const history = useHistory();
  const notes = useSelector(selectNotes);

  const dispatch = useDispatch();

  const addNoteHandler = () => {
    let newId = 1;

    if (notes.length > 0) {
      newId = [...notes].sort((a, b) => b.noteid - a.noteid)[0].noteid + 1;
    }

    dispatch(
      addNote({
        title: 'New note...',
        description: '',
        noteid: newId
      })
    );

    history.push(`/note/${newId}`);
  };

  return (
    <button
      type="button"
      onClick={addNoteHandler}
      title={title}
      className="btn btn-primary mt-3"
    >
      {children}
    </button>
  );
};

export default Button;
