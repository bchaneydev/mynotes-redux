import React, { useState, useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
import Header from '../Header';
import Note from '../Note';
import NoteList from '../NoteList';
import Welcome from '../Welcome';
import _ from 'lodash';
import useBrowserWidth from '../../hooks/useBrowserWidth';

import SplitPane from 'react-split-pane';

const App = () => {
  const [isMobile, setIsMobile] = useState(false);

  const browserWidth = useBrowserWidth(300);

  useEffect(() => {
    browserWidth < 576 ? setIsMobile(true) : setIsMobile(false);
  }, [browserWidth, setIsMobile]);

  return (
    <div className="container-fluid h-100">
      <Header />
      <div className="row content">
        <SplitPane
          allowResize={isMobile ? false : true}
          split={isMobile ? 'horizontal' : 'vertical'}
          minSize={200}
          maxSize={800}
          defaultSize={parseInt(
            localStorage.getItem('splitPos') || '400',
            undefined
          )}
          onChange={_.debounce(
            (size) => localStorage.setItem('splitPos', size.toString()),
            500
          )}
        >
          <NoteList />

          <Switch>
            <Route exact path="/">
              <Welcome />
            </Route>
            <Route path="/note/:id">
              <Note />
            </Route>
            <Route path="*">
              <Welcome />
            </Route>
          </Switch>
        </SplitPane>
      </div>
    </div>
  );
};

export default App;
