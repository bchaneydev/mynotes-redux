import React from 'react';
import { useSelector } from 'react-redux';
import { selectNotes } from '../../selectors';
import { Note } from '~/types';
import NoteListItem from './NoteListItem';

import './style.scss';

const NoteList = () => {
  const notes = useSelector(selectNotes);

  const listNotes = notes
    .map((note: Note) => <NoteListItem key={note.noteid} note={note} />)
    .reverse();

  return (
    <div className="list-col">
      <ul>{listNotes}</ul>
    </div>
  );
};

export default NoteList;
