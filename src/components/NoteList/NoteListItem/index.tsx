import React from 'react';
import { NavLink } from 'react-router-dom';

type Props = {
  note: {
    title: string;
    description: string;
    noteid: number;
  }
};

const NoteListItem = ({ note }: Props) => {
  return (
    <li>
      <NavLink to={`/note/${note.noteid}`} activeClassName="active">
        {note.title}
      </NavLink>
    </li>
  );
};

export default React.memo(NoteListItem);
