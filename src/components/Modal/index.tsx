import React from 'react';
import ReactDOM from 'react-dom';
const modalRoot = document.getElementById('modal') as HTMLElement;

import './style.scss';

type Props = {
  children: React.ReactNode;
}

class Modal extends React.Component {
  el: HTMLElement = document.createElement('div');
  
  constructor(props: Props) {
    super(props);
  }

  componentDidMount() {
    modalRoot.appendChild(this.el);
  }

  componentWillUnmount() {
    modalRoot.removeChild(this.el);
  }

  render() {
    return ReactDOM.createPortal(this.props.children, this.el);
  }
}

export default Modal;
