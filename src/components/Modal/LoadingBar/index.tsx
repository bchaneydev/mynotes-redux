import React from 'react';

import './style.scss';

const LoadingBar = () => {
  return (
    <div
      className="MuiLinearProgress-root MuiLinearProgress-colorPrimary MuiLinearProgress-indeterminate"
      role="progressbar"
    >
      <div className="MuiLinearProgress-bar MuiLinearProgress-barColorPrimary MuiLinearProgress-bar1Indeterminate"></div>
      <div className="MuiLinearProgress-bar MuiLinearProgress-bar2Indeterminate MuiLinearProgress-barColorPrimary"></div>
    </div>
  );
};

export default LoadingBar;
