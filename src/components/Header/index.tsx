import React, { useState, useRef } from 'react';
import { Link, matchPath, useLocation, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { deleteNote } from '../../reducers/Notes';
import Modal from '../Modal';
import ModalSendNote from '../ModalSendNote';

import './style.scss';

const Header = () => {
  const { pathname } = useLocation();
  const noteMatch: any = matchPath<{ id: string }>(pathname, {
    path: '/note/:id'
  });
  const history = useHistory();
  const [showModal, setShowModal] = useState(false);
  const sendNoteBtn = useRef<HTMLButtonElement>(null!);
  const dispatch = useDispatch();

  const showEmailModalHandler = () => {
    setShowModal(true);
  };

  const hideModalHandler = () => {
    setShowModal(false);
    sendNoteBtn.current.focus();
  };

  const deleteNoteHandler = () => {
    dispatch(deleteNote(parseInt(noteMatch.params.id)));
    // redirect to welcome page
    history.push('/');
  };

  const modal = (
    <Modal>
      <div className="modal-wrap">
        <ModalSendNote closeModal={hideModalHandler} />
      </div>
    </Modal>
  );

  return (
    <div className="row header">
      <div className="col-7 py-2 align-self-center">
        <Link to={'/'}>
          <i className="bi bi-journal-text"></i>
          <h1>
            <span>my</span>notes
          </h1>
        </Link>
      </div>
      <div className="col-5 py-2 text-end align-self-center">
        {noteMatch && (
          <div>
            <button
              type="button"
              className="btn btn-primary"
              title="Send this note"
              onClick={showEmailModalHandler}
              ref={sendNoteBtn}
            >
              <i className="bi bi-envelope-fill"></i>
            </button>
            {showModal && modal}

            <button
              type="button"
              className="btn btn-primary"
              title="Delete this note"
              onClick={deleteNoteHandler}
            >
              <i className="bi bi-trash"></i>
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default Header;
