import React, { useEffect, useState, useRef } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { selectNotes } from '../../selectors';
import { editNote } from '../../reducers/Notes';
import Button from '../Form/Button';
import _ from 'lodash';

import './style.scss';

type NoteParams = {
  id: string;
};

const Note = () => {
  let { id } = useParams<NoteParams>();
  const history = useHistory();
  const firstInputEl = useRef(null);

  const notes = useSelector(selectNotes);
  const dispatch = useDispatch();

  const selectedNote = notes.find((note) => note.noteid === parseInt(id));

  const [formData, setFormData] = useState({
    title: '',
    description: '',
    noteid: 0
  });

  const exists = notes.find((el) => el.noteid === parseInt(id));

  useEffect(() => {
    if (selectedNote) {
      document.title = selectedNote.title;
    }

    return () => {
      document.title = 'mynotes';
    };
  }, [selectedNote]);

  useEffect(() => {
    if (exists) {
      setFormData(selectedNote || { title: '', description: '', noteid: 0 });
    } else {
      history.push('/');
    }
  }, [exists, history, selectedNote]);

  const handleBlurChange = () => {
    return _.isEqual(notes[parseInt(id) - 1], formData) ? false : submitForm();
  };

  const handleInputChange = (e: {
    target: HTMLInputElement | HTMLTextAreaElement;
  }) => {
    const field = e.target.id;

    setFormData({
      ...formData,
      [field]: e.target.value
    });
  };

  const submitForm = () => {
    console.log('submitting...');
    if (formData.title === '') {
      formData.title = 'No title...';
    }
    dispatch(editNote(formData));
  };

  return (
    <form className="note-form h-100" onSubmit={(e) => e.preventDefault()}>
      <div className="form-group">
        <label htmlFor="title">Title</label>
        <input
          type="text"
          id="title"
          ref={firstInputEl}
          value={formData.title}
          onChange={handleInputChange}
          onBlur={handleBlurChange}
          className="form-control"
          placeholder="Please enter a title..."
        />
      </div>
      <div className="form-group textarea-wrap">
        <label htmlFor="description">Note</label>
        <textarea
          id="description"
          value={formData.description}
          onChange={handleInputChange}
          onBlur={handleBlurChange}
          className="form-control"
          placeholder="Please enter a description..."
        />
      </div>
      <Button title="New note">
        <i className="bi bi-pencil"></i>
      </Button>
    </form>
  );
};

export default Note;
