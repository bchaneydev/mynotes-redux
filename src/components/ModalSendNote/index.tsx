import React, { useState, useEffect, useRef } from 'react';
import useModalTrap from '../../hooks/useModalTrap';
import ModalSendNoteForm from './ModalSendNoteForm';
import ModalNoteSent from './ModalNoteSent';

const ModalSendNote = ({ closeModal }: { closeModal: () => void }) => {
  const [isFormSent, setIsFormSent] = useState(false);
  const [currentNote, setcurrentNote] = useState({ title: '', description: '', noteid: 0 });
  const [userEmail, setuserEmail] = useState('');
  const modalRef = useRef<HTMLDivElement>(null);
  
  useModalTrap(modalRef, closeModal);

  return (
    <div
      className="modal-inner"
      role="dialog"
      aria-labelledby="modal-label"
      aria-modal="true"
    >
      <div ref={modalRef}>
        <button type="button" className="btn close" onClick={closeModal}>
          <i className="bi bi-x"></i><span className="visually-hidden">Close modal</span>
        </button>
        <h1 id="modal-label">Email this note</h1>
        <div className="m-content">
          {!isFormSent && <ModalSendNoteForm setIsFormSent={setIsFormSent} setcurrentNote={setcurrentNote} setuserEmail={setuserEmail} />}
          {isFormSent && <ModalNoteSent closeModal={closeModal} currentNote={currentNote} userEmail={userEmail} />}
        </div>
      </div>
    </div>
  );
};

export default ModalSendNote;
