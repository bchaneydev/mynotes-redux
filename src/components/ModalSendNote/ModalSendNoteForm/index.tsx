import React, { useState, useEffect, useRef } from 'react';
import { matchPath, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { selectNotes } from '../../../selectors';
import LoadingBar from '../../Modal/LoadingBar';
import emailjs from 'emailjs-com';
import { Note } from '../../../types';

const ModalSendNoteForm = ({
  setIsFormSent,
  setcurrentNote,
  setuserEmail
}: {
  setIsFormSent: React.Dispatch<React.SetStateAction<boolean>>;
  setcurrentNote: React.Dispatch<React.SetStateAction<Note>>;
  setuserEmail: React.Dispatch<React.SetStateAction<string>>;
}) => {
  const { pathname } = useLocation();
  const noteMatch: any = matchPath<{ id: string }>(pathname, {
    path: '/note/:id'
  });
  const notes = useSelector(selectNotes);
  const [formEmail, setFormEmail] = useState('');
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isFormValid, setIsFormValid] = useState(false);
  const emailInputRef = useRef<HTMLInputElement>(null!);

  const currentNote = {
    ...notes.filter((note) => note.noteid === parseInt(noteMatch.params.id))[0]
  };

  const handleInputChange = (e: { target: HTMLInputElement }) => {
    setFormEmail(e.target.value);
  };

  const validateEmail = (email: string) => {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  const submitForm = () => {
    if (!validateEmail(formEmail)) {
      setIsEmailValid(false);
      return;
    }

    setIsEmailValid(true);
    setuserEmail(formEmail);
    setIsFormValid(true);

    emailjs
      .send(
        process.env.REACT_APP_SERVICE_ID as string,
        process.env.REACT_APP_TMPL_ID as string,
        {
          title: currentNote.title,
          message: currentNote.description,
          to: formEmail
        },
        process.env.REACT_APP_USER_ID as string
      )
      .then((res) => {
        console.log('Email succesfully sent');
        setIsFormValid(false);
        setIsFormSent(true);
      })
      .catch((err) =>
        console.error('Email send came back with an error: ', err)
      );
  };

  useEffect(() => {
    setcurrentNote(currentNote);
    emailInputRef.current.focus();
  }, []);

  return (
    <>
      <p>
        Send your note titled &ldquo;<em>{currentNote.title}</em>&rdquo; to:
      </p>
      {!isEmailValid && (
        <p className="error-red">Please enter a valid email address below</p>
      )}
      <form onSubmit={(e) => e.preventDefault()}>
        <div className="form-group">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            id="email"
            value={formEmail}
            onChange={handleInputChange}
            className="form-control"
            placeholder="Please enter an email..."
            ref={emailInputRef}
          />
        </div>
        <button
          type="button"
          className="btn btn-primary"
          id="btn-send"
          title="Email this note"
          onClick={submitForm}
          disabled={isFormValid}
        >
          Send note
        </button>
        {isFormValid && <LoadingBar />}
      </form>
    </>
  );
};

export default ModalSendNoteForm;
