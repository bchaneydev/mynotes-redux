import React from 'react';
import { Note } from '../../../types';

const ModalNoteSent = ({ closeModal, currentNote, userEmail } : { closeModal: () => void, currentNote: Note, userEmail: string }) => {
  return (
    <>
      <p>You have successfully sent your note titled &ldquo;<em>{currentNote.title}</em>&rdquo; to {userEmail}.</p>
      <button
        type="button"
        className="btn btn-primary"
        id="btn-send"
        title="Close"
        onClick={closeModal}
      >
        Close
      </button>
    </>
  )
}

export default ModalNoteSent;