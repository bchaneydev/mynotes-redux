import { useLayoutEffect, useState } from 'react';
import _ from 'lodash';

const useBrowserWidth = (delay = 500) => {
  const [width, setWidth] = useState(window.innerWidth);

  useLayoutEffect(() => {
    const debouncedWidth = _.debounce(() => setWidth(window.innerWidth), delay);

    window.addEventListener('resize', debouncedWidth);

    debouncedWidth();

    return () => {
      // clean up
      window.removeEventListener('resize', debouncedWidth);
      // cancel debounce as well
      debouncedWidth.cancel;
    };
  }, [setWidth]);

  return width;
};

export default useBrowserWidth;
