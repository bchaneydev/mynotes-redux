import React, { useEffect, useRef, MutableRefObject } from 'react';

const useModalTrap = (
  modalRef: React.Ref<HTMLDivElement>,
  closeModal: () => void
) => {
  let activeIndex = useRef(1);
  let focusableElements: MutableRefObject<HTMLCollectionBase> = useRef(null!);

  useEffect(() => {
    if (modalRef) {
      // select all focusable elements within ref
      focusableElements.current = (
        modalRef as MutableRefObject<HTMLDivElement>
      ).current.querySelectorAll('a, button, textarea, input, select');
    }
  }, [modalRef]);

  useEffect(() => {
    const handleTab = (e: KeyboardEvent) => {
      let total = focusableElements.current.length;

      // if tab was pressed without shift
      if (!e.shiftKey) {
        // if activeIndex + 1 larger than array length focus first element otherwise focus next element
        activeIndex.current + 1 === total
          ? (activeIndex.current = 0)
          : (activeIndex.current += 1);

        (focusableElements.current[activeIndex.current] as HTMLElement).focus();

        // don't do anything I wouldn't do
        return e.preventDefault();
      }

      // if tab was pressed with shift
      if (e.shiftKey) {
        // if activeIndex - 1 less than 0 focus last element otherwise focus previous element
        activeIndex.current - 1 < 0
          ? (activeIndex.current = total - 1)
          : (activeIndex.current -= 1);

        (focusableElements.current[activeIndex.current] as HTMLElement).focus();

        // don't do anything I wouldn't do
        return e.preventDefault();
      }
    };

    const handleEscape = (e: KeyboardEvent) => {
      if (closeModal && e.key === 'Escape') {
        closeModal();
      }
    };

    // map of keyboard listeners
    const keyListenersMap = new Map([
      ['Escape', handleEscape],
      ['Tab', handleTab]
    ]);

    const handleKeyDown = (e: KeyboardEvent) => {
      // get the listener corresponding to the pressed key
      const listener = keyListenersMap.get(e.key);

      // call the listener if it exists
      return listener && listener(e);
    };

    window.addEventListener('keydown', handleKeyDown);

    return () => {
      window.removeEventListener('keydown', handleKeyDown);
    };
  }, [closeModal]);
};

export default useModalTrap;
